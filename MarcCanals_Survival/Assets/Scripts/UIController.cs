﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public TextMeshProUGUI textDead;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        textDead.text = GameManager.Instance.textDeath;
    }

    public void RestartScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    public void BackToMenu() {

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
