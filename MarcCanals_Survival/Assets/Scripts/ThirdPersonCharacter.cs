using Cinemachine;
using UnityEngine;

	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CharacterController))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class ThirdPersonCharacter : MonoBehaviour
	{
		[SerializeField] float m_MovingTurnSpeed = 360;
		[SerializeField] float m_StationaryTurnSpeed = 180;
		[SerializeField] float m_JumpPower = 12f;
		[Range(1f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
    [SerializeField] float m_MoveSpeedMultiplier = 1f, speed;
		[SerializeField] float m_AnimSpeedMultiplier = 1f;
		[SerializeField] float m_GroundCheckDistance = 0.1f;
	public CinemachineVirtualCamera DanceCam;
		Rigidbody m_Rigidbody;
		Animator m_Animator;
		CharacterController m_CharController;
		bool m_IsGrounded, animIsWalking, animIsRunning, animIsJumping, animIsCrouching, animIsDancing, animIsTaking, animIsGun;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		float m_TurnAmount;
		float m_ForwardAmount;
		Vector3 m_GroundNormal;
		float m_CapsuleHeight;
		Vector3 m_CapsuleCenter;
		CapsuleCollider m_Capsule;
		bool m_Crouching;
    public Inventory inventory;
    public GameObject itemHolder;

		void Start()
		{
			m_Animator = GetComponent<Animator>();
			m_Rigidbody = GetComponent<Rigidbody>();
			m_Capsule = GetComponent<CapsuleCollider>();
			m_CharController = GetComponent<CharacterController>();
			m_CapsuleHeight = m_Capsule.height;
			m_CapsuleCenter = m_Capsule.center;

			m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			m_OrigGroundCheckDistance = m_GroundCheckDistance;

        }


	public void Move(Vector3 move, bool crouch, bool jump, bool grab, bool aim, bool dance, bool outGun)
	{
		if (!animIsDancing)
		{
			if (!dance)
			{
				// convert the world relative moveInput vector into a local-relative
				// turn amount and forward amount required to head in the desired
				// direction.

				//Sprint stuff
				if (Input.GetKey(KeyCode.LeftShift))
				{
					m_MoveSpeedMultiplier = 2;
					animIsRunning = true;
				}
				else
				{
					animIsRunning = false;
					m_MoveSpeedMultiplier = 1;
				}
				if (move != Vector3.zero)
				{
					animIsWalking = true;
				}
				else
				{
					animIsWalking = false;
				}

				if (grab)
				{
					animIsTaking = true;
				}
				else
				{
					animIsTaking = false;
				}
				if (outGun)
				{
					animIsGun = true;
					m_Animator.SetLayerWeight(1, 1);
				}
				else
				{
					m_Animator.SetLayerWeight(1, 0);
					animIsGun = false;
				}
				m_CharController.Move(move * m_MoveSpeedMultiplier * speed * Time.deltaTime);
                
				if (move.magnitude > 1f) move.Normalize();
				move = transform.InverseTransformDirection(move);
				CheckGroundStatus();
				move = Vector3.ProjectOnPlane(move, m_GroundNormal);
				m_TurnAmount = Mathf.Atan2(move.x, move.z);
				m_ForwardAmount = move.z;

				ApplyExtraTurnRotation();

				// control and velocity handling is different when grounded and airborne:
				if (m_IsGrounded)
				{
					HandleGroundedMovement(crouch, jump);
                    m_Rigidbody.velocity = Vector3.zero;
                    
                }
				else
				{
				
                 //   HandleAirborneMovement();
                    
				}

				ScaleCapsuleForCrouching(crouch);
			}
			else
			{
				animIsDancing = true;
			}

			// update animator
			UpdateAnimator();

		}
		else {
			DanceCam.Priority = 20;
		}
	}

    public void AddItemToInventory(Item item) {
        if (item.DamagePlayer) { GameManager.Instance.killPlayer(item.textDead); }
        inventory.items.Add(item);
        itemHolder.GetComponent<MeshFilter>().mesh = item.mesh;
        itemHolder.GetComponent<MeshRenderer>().material = item.meshMat;
         }
		void ScaleCapsuleForCrouching(bool crouch)
		{
			if (m_IsGrounded && crouch)
			{
				if (m_Crouching) return;
				m_Capsule.height = m_Capsule.height / 2f;
				m_Capsule.center = m_Capsule.center / 2f;
				animIsCrouching = true;
				m_Crouching = true;
			}
			else
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
					return;
				}
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				animIsCrouching = false;
				m_Crouching = false;
			}
		}

		void UpdateAnimator()
		{
			// update the animator parameters
			m_Animator.SetBool("isWalking", animIsWalking);
			m_Animator.SetBool("isRunning", animIsRunning);
			m_Animator.SetBool("isJumping", animIsJumping);
			m_Animator.SetBool("isCrouching", animIsCrouching);
			m_Animator.SetBool("isTaking", animIsTaking);
			m_Animator.SetBool("isDancing", animIsDancing);
			m_Animator.SetBool("gunOut", animIsGun);


		}

		/*
		void HandleAirborneMovement()
		{
			// apply extra gravity from multiplier:
			Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
			m_Rigidbody.AddForce(extraGravityForce);
        Debug.Log("Handling airbone movement");
        m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.1f;
		}
		*/

		void HandleGroundedMovement(bool crouch, bool jump)
		{
			// check whether conditions are right to allow a jump:
			// && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded")
			if (jump && !crouch)
			{
            // jump!
            Debug.Log("Handling grounded movement");
				m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
				m_IsGrounded = false;
				m_Animator.applyRootMotion = false;
				m_GroundCheckDistance = 0.1f;
				animIsJumping = true;
			}
		}

		void ApplyExtraTurnRotation()
		{
			// help the character turn faster (this is in addition to root rotation in the animation)
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		}

		/*public void OnAnimatorMove()
		{
			// we implement this function to override the default root motion.
			// this allows us to modify the positional speed before it's applied.
			if (m_IsGrounded && Time.deltaTime > 0)
			{
				Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

				// we preserve the existing y part of the current velocity.
				v.y = m_Rigidbody.velocity.y;
				m_Rigidbody.velocity = v;
			}
		}
        */


		void CheckGroundStatus()
		{
			RaycastHit hitInfo;
#if UNITY_EDITOR
			// helper to visualise the ground check ray in the scene view
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
			// 0.1f is a small offset to start the ray from inside the character
			// it is also good to note that the transform position in the sample assets is at the base of the character
			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
			{

				m_GroundNormal = hitInfo.normal;
				m_IsGrounded = true;
				m_Animator.applyRootMotion = true;
			}
			else
			{
				m_IsGrounded = false;
				m_GroundNormal = Vector3.up;
				m_Animator.applyRootMotion = false;
			}
		}
	private void finishDance()
	{
		animIsDancing = false;
		DanceCam.Priority = 10;
	}
	private void finishJump()
	{
		animIsJumping = false;
	}
}

   
