﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFollowPlayer : MonoBehaviour
{
    public VisionControll controllerVision;

    private StateMachine stateMachine;
    private ControllerNav controllerNavMesh;
    void Start()
    {
        controllerNavMesh = GetComponent<ControllerNav>();
        //controllerVision = GetComponent<ControllerVision>();
        stateMachine = GetComponent<StateMachine>();
    }

    void Update()
    {
   

        if (!controllerVision.EnemySeePlayer())
        {
         
            return;
        }

        if (controllerNavMesh.EnemyArriveToPos())
        {
            controllerNavMesh.EnemyStopNavMesh();
            stateMachine.Activate(stateMachine.stateAttack);
            return;
        }

        controllerNavMesh.EnemyMovementnavMeshPlayer();
    }

    private void OnEnable()
    {
        //stateMachine.animation = "TriggerMove";
        //controllerNavMesh.EnemyMovementnavMeshPlayer();
    }
}
