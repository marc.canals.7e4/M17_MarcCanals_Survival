﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatePatroll : MonoBehaviour
{
    public GameObject[] WayPoints;
    public VisionControll controllerVision;
    private StateMachine stateMachine;
    public Animator animator;
    private ControllerNav controllerNavMesh;
    private int nextWayPoint;
    // Start is called before the first frame update
    void Start()
    {
        stateMachine = GetComponent<StateMachine>();
        animator = GetComponent<Animator>();
        controllerNavMesh = GetComponent<ControllerNav>();
        WayPoints = GameObject.FindGameObjectsWithTag("Waypoints");
        animator.SetBool("Run", true);
        VerifyWayPointDestinity();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (controllerVision.EnemySeePlayer())
        {
            controllerNavMesh.player = controllerVision.player;
            stateMachine.Activate(stateMachine.stateFollow);
            return; 
        }

       
        if (controllerNavMesh.EnemyArriveToPos())
        {
            animator.SetBool("Run", false);
            nextWayPoint = Random.Range(0, WayPoints.Length);
            VerifyWayPointDestinity();
        }
    }

    void VerifyWayPointDestinity()
    {
        controllerNavMesh.EnemyMovementnavMesh(WayPoints[nextWayPoint].transform.position);
        animator.SetBool("Run", true);
    }


}
