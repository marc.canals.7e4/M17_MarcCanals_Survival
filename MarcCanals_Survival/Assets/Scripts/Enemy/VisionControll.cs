﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionControll : MonoBehaviour
{
    private StateMachine state;
    private ControllerNav controller;
    private bool seePlayer;
    public Transform player;
    public SphereCollider sphereCollider;
    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.Instance.Player.transform;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool EnemySeePlayer()
    {
        Debug.Log(seePlayer);
        return seePlayer;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) seePlayer = true;

    }

    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            player = other.gameObject.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) seePlayer = false;
    }
}
