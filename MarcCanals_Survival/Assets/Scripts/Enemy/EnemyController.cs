﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] waypoints;
    private GameObject player;
    private Animator animator;
    private bool isChasing, isPatrolling, isPunching;
    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.Instance.Player;
        animator = GetComponent<Animator>();
        chasePlayer();
    }

    // Update is called once per frame
    void Update()
    {
        if (isChasing)
        {
            chasePlayer();
        }

        if (isPatrolling) {
            Patrolling();
        }

            if (Input.GetKeyDown(KeyCode.Return)) {
            if (isChasing) {
                Patrolling();
                return;
            }
            if (isPatrolling)
            {
                chasePlayer();
                return;
            }
        }

        if (EnemyArriveToPos() && isPatrolling) {
            animator.SetBool("Run",false);
            animator.SetBool("Punch", false);
        }

        if (EnemyArriveToPos() && isChasing && !isPunching)
        {
            isPunching = true;
            animator.SetBool("Punch", true);
            StartCoroutine(PunchReset());
        }
        else if (isChasing) {
            animator.SetBool("Punch", false);
        }
    }
    public IEnumerator PunchReset(){
        yield return new WaitForSeconds(2f);
        animator.SetBool("Punch", false);
        isPunching = false;
    }
    public void chasePlayer()
    {
        isChasing = true;
        isPatrolling = false;
        this.gameObject.GetComponent<NavMeshAgent>().destination = player.transform.position;
        animator.SetBool("Run", true);
    }  
    public void Patrolling()
    {
        isChasing = false;
        isPatrolling = true;
        this.gameObject.GetComponent<NavMeshAgent>().destination = waypoints[0].transform.position;
        animator.SetBool("Run", true);
    }

    public bool EnemyArriveToPos()
    {
        return this.gameObject.GetComponent<NavMeshAgent>().remainingDistance <= this.gameObject.GetComponent<NavMeshAgent>().stoppingDistance && !this.gameObject.GetComponent<NavMeshAgent>().pathPending; 
    }
}
