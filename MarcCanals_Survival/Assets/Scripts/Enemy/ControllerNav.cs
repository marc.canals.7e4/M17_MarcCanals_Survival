﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControllerNav : MonoBehaviour
{
    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        player = GameManager.Instance.Player.transform;
    }
    public void EnemyMovementnavMeshPlayer()
    {
        EnemyMovementnavMesh(player.transform.position);
    }
    public void EnemyMovementnavMesh(Vector3 pointToFollow)
    {
        this.gameObject.GetComponent<NavMeshAgent>().destination = pointToFollow;
        this.gameObject.GetComponent<NavMeshAgent>().isStopped = false;
    }

    public void EnemyStopNavMesh()
    {
        this.gameObject.GetComponent<NavMeshAgent>().isStopped = true;
    }
    public bool EnemyArriveToPos()
    {
        return this.gameObject.GetComponent<NavMeshAgent>().remainingDistance <= this.gameObject.GetComponent<NavMeshAgent>().stoppingDistance && !this.gameObject.GetComponent<NavMeshAgent>().pathPending;
    }
}
