﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAttack : MonoBehaviour
{
    public StateMachine state;
    public ControllerNav controller;
    public Animator animator;
    public bool isAttacking = true;
    public VisionControll controllerVision;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!controller.EnemyArriveToPos()) {
            
            state.Activate(state.stateFollow);
        }


        if (!controllerVision.EnemySeePlayer())
        {
            animator.SetBool("Run", true);
            isAttacking = false;
            state.Activate(state.statePatrolling);
        }
    }
    private void OnEnable()
    {
        isAttacking = true;
        ThrowPunch();
    }

    public void ThrowPunch() {
        
        if (isAttacking) { 
            StartCoroutine(PunchDelay());
            animator.SetBool("Punch", true);
        }
        
    }
    public IEnumerator PunchDelay() {
        yield return new WaitForSeconds(0.5f);
        Debug.Log("Im hitting");
        animator.SetBool("Punch", false);
        yield return new WaitForSeconds(2f);
        ThrowPunch();
    }
}
