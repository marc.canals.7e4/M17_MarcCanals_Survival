﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [Header("States parameters")]
    public MonoBehaviour stateFollow;
    public MonoBehaviour statePatrolling;
    public MonoBehaviour stateAttack;
    public MonoBehaviour stateInitial;

    private MonoBehaviour stateActual;
    public Animator enemyAnimatior;
    // Start is called before the first frame update
    void Start()
    {
        Activate(stateInitial);
    }

    public void Activate(MonoBehaviour state)
    {
            if (stateActual != null) stateActual.enabled = false;
            stateActual = state;
            stateActual.enabled = true;
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
