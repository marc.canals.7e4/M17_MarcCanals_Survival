﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/InventoryData")]
public class Inventory : ScriptableObject
{
    public ScriptableObject gun;
    public List<Item> items;
}
