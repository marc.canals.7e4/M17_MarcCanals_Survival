﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/ItemData")]
public class Item : ScriptableObject
{
    public string name;
    public Mesh mesh;
    public Material meshMat;
    public bool isKey;
    public bool DamagePlayer;
    public string textDead;

}
