﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pickable : MonoBehaviour
{
    public TextMeshProUGUI text;
    private bool isInteractable;
    public Item itemData;
    public ThirdPersonCharacter player;
    public AudioSource audioItem;
    // Start is called before the first frame update
    void Start()
    {
        audioItem = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<ThirdPersonCharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        { isInteractable = true; }
        else { isInteractable = false; }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            text.gameObject.SetActive(true);


        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (isInteractable)
        {
            audioItem.Play();
            //TODO: Item collecting
            player.AddItemToInventory(itemData);
            isInteractable = false;
            Destroy(this.gameObject, 0.2f);
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            text.gameObject.SetActive(false);
        }

    }
}
